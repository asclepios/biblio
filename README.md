# Epione, Asclepios and Epidaure projects bibliography

This repository contains files needed to generate (and update) the web page
https://team.inria.fr/epione/publications which in fact contains an iframe
pointing to https://www-sop.inria.fr/epione/biblio/index.html

# What are the different files?

## *.cfg files

These are files bibtex2html configuration files. It lists the authors that are
indexed on the generated html files, and helps with authors whose name isn't
consistent in all publications.

## *.html and publis.css

These files contains formatting options for the output html pages.

## bib-archive/*.bib

This is the archived bibliography. It is supposed to contain publications for
years Y - 2 and before. It must be manually adjusted at the end of each year.
Newer publications are retrieved on HAL via the `update.wwwsite` script that is
executed on `srv-epione`.

## update.client

This script propagates the changes made to the source and updates the html
pages.

## update.wwwsite

This script is meant to be run on srv-epione and updates the html pages. It
is automatically launched by `update.client` if you have the right permissions,
i.e., ssh access as epione-publis on srv-epione. It also ran twice a day
automatically through a cron job on srv-epione.

It contains the important variables `$PUBLI_DEBUT` and $`PUBLI_FIN` that
determines whithin which timeframe publications should be updated from HAL,
as opposed to publications archived in `bib-archive/*.bib`.

## old/*

This is here for historical reasons and shouldn't be needed.
