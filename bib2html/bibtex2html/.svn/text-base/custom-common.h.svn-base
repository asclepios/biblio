/*************************************************************************
 * custom-common.h - from bibtex2html distribution
 *
 * $Id: custom-common.h,v 2.6 2004/05/06 13:12:26 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */



#ifndef _custom_common_h_
#define _custom_common_h_

#ifdef __cplusplus
extern "C" {
#endif



#include <stdio.h>
#include <stdlib.h>
#include <biblio.h>

#include <common.h>




/* some predefined styles
 */
typedef enum {
  _NO_STYLE_,
  _BIBTEX_,
  _MEDLINE_,
  _HTML_,
  _LATEX_,
  _LAYOUT_,
  _TXT_
} enumPredefinedStyle;



/* type of arguments describing
   environment variable
*/
typedef enum {
  _TABLE_,
  _ENV_,
  _TAG_,
  _STRING_,
  _INT_
} enumTypeArg;



typedef struct {
  char key[SHORT_STR_LENGTH];
  enumTypeArg type;
  void *arg;
  /*   int allocated_length; */
} typeArgDescription;



typedef struct {
  char * start;
  char * end;
} typeTagElement;



typedef struct {
  int use_icon;
  char *icon;
  typeTagElement link_field;
  char *link;
  typeTagElement content_field;
  char *prefix;
  typeTagElement content;
} typeEnvElement;



typedef struct {
  typeTagElement table;
  typeTagElement row;
  typeTagElement cell;
  typeTagElement empty_cell;
  typeTagElement arg1_in_cell;
  typeTagElement arg2_in_cell;
  int cells_per_row;
  typeTagElement extra_cell;
  typeTagElement empty_extra_cell;
} typeTableElement;



extern int add_tag_element( typeArgDescription *custom,
		     int length, char *key,
		     typeTagElement *tag );

extern void init_tag_element( typeTagElement *t );

extern void free_tag_element( typeTagElement *t );

extern void print_tag_element( FILE *file, char *key, 
			       typeTagElement *tag );



extern int add_env_element( typeArgDescription *custom,
		     int length, char *key,
		     typeEnvElement *env );

extern void init_env_element( typeEnvElement *e );

extern void free_env_element( typeEnvElement *e );

extern void print_env_element( FILE *file, char *key, 
			       typeEnvElement *e );


extern int add_table_element( typeArgDescription *custom,
		     int length, char *key,
		     typeTableElement *table );

extern void init_table_element( typeTableElement *t );

extern void free_table_element( typeTableElement *t );

extern void print_table_element( FILE *file, char *key, 
			       typeTableElement *t );
#ifdef __cplusplus
}
#endif

#endif
