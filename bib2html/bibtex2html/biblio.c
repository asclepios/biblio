/*************************************************************************
 * biblio.c - from bibtex2html distribution
 *
 * $Id: biblio.c,v 2.13 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#include <biblio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* OK, those are global variables,
   and I know that I should avoid them, but...

   and ok, some of them could be only local
   but I prefer to group them into a single file
*/




/**************************************************
 *
 *   ALL WHAT I NEED FOR WRITING
 *
 **************************************************/

const char *desc_sort_criterium[] = { "name", "category", "type", "year", "title", "bibkey", "" };



/* Categories (MUST BE in order with respect to enumTypeCategory)
 */

const char *desc_category[] = { "Books and proceedings",  "Thesis",
				"Articles in journal, book chapters",
				"Conference articles",
				"Internal reports", "Patents, standards",
				"Manuals, booklets",
				"Miscellaneous", "Unknown" };

const char *filename_category[] = { "books",  "thesis", "articles",
				    "conferences", "reports", "patentsstandards",
				    "manuals", "misc", "UNKNOWN" };








/**************************************************
 *
 *   ALL WHAT I NEED FOR READING
 *
 **************************************************/


/* item key description (MUST BE in order with respect to enumTypeItem)
*/
const char *desc_item[]  = { "ARTICLE",      "BOOK",          "BOOKLET",   "CONFERENCE",    "INBOOK",
			     "INCOLLECTION", "INPROCEEDINGS", "MANUAL",    "MASTERSTHESIS", "MISC",
			     "PHDTHESIS",    "PROCEEDINGS",   "TECHREPORT","UNPUBLISHED",

			     "PATENT", "STANDARD",

			     "XXX" };




/* field key description (MUST BE in order with respect to enumTypeField in biblio.h)
   do not forget to correct DESC_FIELD_NB accordingly
*/
const char *desc_field[]  = { "ADDRESS",          "ALT",            "ANNOTE",         "AUTHOR",        "BOOKTITLE",
			      "CHAPTER",          "CROSSREF",       "EDITION",        "EDITOR",        "HOWPUBLISHED", 
			      "INSTITUTION",      "JOURNAL",        "KEY",            "MONTH",         "NOTE",       
			      "NUMBER",           "OPT",            "ORGANIZATION",   "PAGES",         "PUBLISHER",  
			      "SCHOOL",           "SERIES",         "TITLE",          "TYPE",          "VOLUME",   
			      "YEAR",             "NON-EXISTING",   "NON-EXISTING",   "NON-EXISTING",  "NON-EXISTING",

			      "ABSTRACT",         "CITEKEY",        "COMMENTS",       "DOI",           "ISBN",
			      "ISSN",             "KEYWORD",        "KEYWORDS",       "URL",           "URL-PUBLISHER",   
			      "PDF",              "PMID",           "POSTSCRIPT",     "PS",            "NON-EXISTING",
#if (defined(_INRIA_) || defined(_HALINRIA_))
			      "X-EDITORIAL-BOARD", "X-INTERNATIONAL-AUDIENCE", "X-INVITED-CONFERENCE", "X-PAYS", "X-PROCEEDINGS",
                              "X-SCIENTIFIC-POPULARIZATION", "NON-EXISTING", "NON-EXISTING", "NON-EXISTING","NON-EXISTING",
			      
			      "HAL-DATE-DEPOT",   "URL-HAL",        "X-ID-HAL",       "HAL-VERSION",  "X-LANGUAGE",
#endif
#if defined(_HALINRIA_) 
			      "FILE",             "WRITING_DATE",   "NON-EXISTING",   "NON-EXISTING",  "NON-EXISTING",
#endif
			      "XXX" };



const int desc_field_lgth[] = { STD_STR_LENGTH,   0,                LONG_STR_LENGTH,  LONG_STR_LENGTH,   STD_STR_LENGTH,
				STD_STR_LENGTH,   SHORT_STR_LENGTH, SHORT_STR_LENGTH, STD_STR_LENGTH,    STD_STR_LENGTH,  
				STD_STR_LENGTH,   STD_STR_LENGTH,   SHORT_STR_LENGTH, SHORT_STR_LENGTH,  STD_STR_LENGTH,   
				STD_STR_LENGTH,   0,                STD_STR_LENGTH,   SHORT_STR_LENGTH,  STD_STR_LENGTH,   
		                STD_STR_LENGTH,   STD_STR_LENGTH,   STD_STR_LENGTH,   STD_STR_LENGTH,    SHORT_STR_LENGTH, 
				SHORT_STR_LENGTH, 0,                0,                0,                 0
				,
				LONG_STR_LENGTH,  STD_STR_LENGTH,   LONG_STR_LENGTH,  SHORT_STR_LENGTH,  SHORT_STR_LENGTH,  
				SHORT_STR_LENGTH, LONG_STR_LENGTH,  LONG_STR_LENGTH,  STD_STR_LENGTH,    STD_STR_LENGTH, 
				STD_STR_LENGTH,   SHORT_STR_LENGTH, STD_STR_LENGTH,   STD_STR_LENGTH,   0
				,
#if (defined(_INRIA_) || defined(_HALINRIA_))
				SHORT_STR_LENGTH, SHORT_STR_LENGTH, SHORT_STR_LENGTH, SHORT_STR_LENGTH, SHORT_STR_LENGTH,
				SHORT_STR_LENGTH, 0,                0,                0,                 0
				,
				SHORT_STR_LENGTH, STD_STR_LENGTH,   SHORT_STR_LENGTH, SHORT_STR_LENGTH, SHORT_STR_LENGTH
#endif
#if defined(_HALINRIA_)
	        		,
				LONG_STR_LENGTH,  SHORT_STR_LENGTH, 0,                0,                 0
#endif
				};
  

/* 
 * fields are
"ADDRESS",          "ALT",            "ANNOTE",         "AUTHOR",        "BOOKTITLE",
"CHAPTER",          "CROSSREF",       "EDITION",        "EDITOR",        "HOWPUBLISHED", 
"INSTITUTION",      "JOURNAL",        "KEY",            "MONTH",         "NOTE",       
"NUMBER",           "OPT",            "ORGANIZATION",   "PAGES",         "PUBLISHER",  
"SCHOOL",           "SERIES",         "TITLE",          "TYPE",          "VOLUME",   
"YEAR",             "NON-EXISTING",   "NON-EXISTING",   "NON-EXISTING",  "NON-EXISTING",

"ABSTRACT",         "CITEKEY",        "COMMENTS",       "DOI",           "ISBN",
"ISSN",             "KEYWORD",        "KEYWORDS",       "URL",           "URLPUBLISHER",   
"PDF",              "PMID",           "POSTSCRIPT",     "PS",            "NON-EXISTING",

#if (defined(_INRIA_) || defined(_HALINRIA_))
"X-EDITORIAL-BOARD", "X-INTERNATIONAL-AUDIENCE", "X-INVITED-CONFERENCE", "X-PAYS", "X-PROCEEDINGS",
"X-SCIENTIFIC-POPULARIZATION", "NON-EXISTING", "NON-EXISTING", "NON-EXISTING","NON-EXISTING",

"HAL-DATE-DEPOT",   "URL-HAL",         "HAL-IDENTIFIANT", "HAL-VERSION",  "XLANGUAGE",
#endif
#if defined(_HALINRIA_) 
"FILE",             "WRITING_DATE",   "NON-EXISTING",   "NON-EXISTING",  "NON-EXISTING",
#endif

"XXX"

* types are
 "ARTICLE",      "BOOK",          "BOOKLET",   "CONFERENCE",    "INBOOK",
 "INCOLLECTION", "INPROCEEDINGS", "MANUAL",    "MASTERSTHESIS", "MISC",
 "PHDTHESIS",    "PROCEEDINGS",   "TECHREPORT","UNPUBLISHED",
 
 "PATENT", "STANDARD",
 
 "XXX"

*/

enumTypeBibtexField desc_fields_bibtex_item[][DESC_FIELD_NB] = {
  /* ARTICLE */
  { _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _REQUIRED_, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#if defined(_HALINRIA_) 
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _REQUIRED_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_) 
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* BOOK */
#if defined(_HALINRIA_)
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
#else
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _ALTERNAT_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _OPTIONAL_, _ALTERNAT_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#if defined(_HALINRIA_)
    _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_,
#endif
    _IGNORED__, _OPTIONAL_, _REQUIRED_, _IGNORED__, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* BOOKLET */
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* CONFERENCE */
#if defined(_HALINRIA_)
  { _REQUIRED_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
#else
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_,
    _IGNORED__, _OPTIONAL_, _REQUIRED_, _IGNORED__, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _REQUIRED_, _OPTIONAL_, _REQUIRED_, _REQUIRED_,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* INBOOK */
#if defined(_HALINRIA_)
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
    _ALTERNAT_, _IGNORED__, _OPTIONAL_, _REQUIRED_, _IGNORED__,
#else
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _ALTERNAT_, _IGNORED__,
    _ALTERNAT_, _IGNORED__, _OPTIONAL_, _ALTERNAT_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _ALTERNAT_, _REQUIRED_,
    _IGNORED__, _OPTIONAL_, _REQUIRED_, _OPTIONAL_, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* INCOLLECTION */
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
#if defined(_HALINRIA_)
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _REQUIRED_, _IGNORED__,
#else
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _REQUIRED_,
    _IGNORED__, _OPTIONAL_, _REQUIRED_, _OPTIONAL_, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },


  /* INPROCEEDINGS */
#if defined(_HALINRIA_)
  { _REQUIRED_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
#else
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_,
    _IGNORED__, _OPTIONAL_, _REQUIRED_, _IGNORED__, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _REQUIRED_, _OPTIONAL_, _REQUIRED_, _REQUIRED_,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* MANUAL */
#if defined(_HALINRIA_)
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
#else
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__, _IGNORED__,
#if defined(_HALINRIA_)
    _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#else
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#endif
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* MASTERSTHESIS */
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _REQUIRED_, _OPTIONAL_, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* MISC */
#if defined(_HALINRIA_)
  { _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
#else
  { _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_,
#if defined(_HALINRIA_)
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _REQUIRED_,
#else
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
#if defined(_HALINRIA_)
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#else
    _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#endif
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },
  
  /* PHDTHESIS */
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _REQUIRED_, _OPTIONAL_, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* PROCEEDINGS */
#if defined(_HALINRIA_)
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
#else
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _IGNORED__,
#endif
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#if defined(_HALINRIA_)
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _REQUIRED_, _REQUIRED_,
#else
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _IGNORED__, _OPTIONAL_,
#endif
    _IGNORED__, _OPTIONAL_, _REQUIRED_, _IGNORED__, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _REQUIRED_, _REQUIRED_, _REQUIRED_, _REQUIRED_,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* TECHREPORT */
  { _OPTIONAL_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
#if defined(_HALINRIA_)
    _IGNORED__, _IGNORED__, _REQUIRED_, _REQUIRED_, _IGNORED__,
#else
    _IGNORED__, _IGNORED__, _REQUIRED_, _OPTIONAL_, _IGNORED__,
#endif
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* UNPUBLISHED */
  { _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _OPTIONAL_, _REQUIRED_,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _IGNORED__,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#endif
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* PATENT */
  { _REQUIRED_, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
#if defined(_HALINRIA_)
    _REQUIRED_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#else
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
#endif
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* STANDARD */
  { _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _REQUIRED_, _OPTIONAL_,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__, _IGNORED__,
    _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  },

  /* UNKNOWN_TYPE_ITEM */
  { _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__,
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _OPTIONAL_, _OPTIONAL_,
    _OPTIONAL_, _IGNORED__, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, 
    _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
#if (defined(_INRIA_) || defined(_HALINRIA_))
    ,
    _IGNORED__, _IGNORED__, _IGNORED__, _REQUIRED_, _IGNORED__,
    _OPTIONAL_, _IGNORED__, _IGNORED__, _IGNORED__, _IGNORED__
    ,
    _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _OPTIONAL_, _REQUIRED_
#endif
#if defined(_HALINRIA_) 
    ,
    _REQUIRED_, _REQUIRED_, _IGNORED__, _IGNORED__, _IGNORED__
#endif
  }

};

/* these fields are recognized but not processed yet.
   If one want to do so,
   he has to add the one he wants to
   enumTypeField, *desc_field[] and desc_field_lgth[]
*/

const char *other_desc_field[] = { "ARCHIVE",  "CITES",    "LOCATION", "NICKNAME", "PRECEDES", 
				   "RAPACT",   "RAPPACT",  "SITE",     "SUCCEEDS", "UPDATE",   
				   "MODIFIED", "OWNER",   "TIMESTAMP", "AFFILIATION", "XXX" };





/* recognized file extensions for FILE field
 */
const char *recognized_file_extensions[] = { ".pdf", ".ps.gz", ".ps", "XXX" };





/* predefined strings for months
 */

const char *month_names_abbr[] = { "jan", "feb", "mar", "apr", "may", "jun",
				   "jul", "aug", "sep", "oct", "nov", "dec" };
const char *month_names_eng[] = { "January", "February", "March",     "April",   "May",      "June",
				  "July",    "August",   "September", "October", "November", "December" };





/* special separator for names
   (beginning by a capital letter)
 */
const char *name_special_separator[] = { "Van", "XXX" };





/* translation of latex into 8 bits characters
 */

const char *latex_to_8bits[][2] = { {"\\`A", "�"},  {"\\`{A}", "�"},
				    {"\\'A", "�"},  {"\\'{A}", "�"}, 
				    {"\\^A", "�"},  {"\\^{A}", "�"}, 
				    {"\\~A", "�"},  {"\\~{A}", "�"},
				    {"\\\"A", "�"}, {"\\\"{A}", "�"},
				    {"\\AA ", "�"}, {"\\AA", "�"}, {"\\A{A}", "�"},
				    {"\\`E", "�"},  {"\\`{E}", "�"}, 
				    {"\\'E", "�"},  {"\\'{E}", "�"},
				    {"\\^E", "�"},  {"\\^{E}", "�"}, 
				    {"\\\"E", "�"}, {"\\\"{E}", "�"}, 
				    {"\\`I", "�"},  {"\\`{I}", "�"}, 
				    {"\\'I", "�"},  {"\\'{I}", "�"}, 
				    {"\\^I", "�"},  {"\\^{I}", "�"}, 
				    {"\\\"I", "�"}, {"\\\"{I}", "�"}, 
				    {"\\~N", "�"},  {"\\~{N}", "�"},
				    {"\\`O", "�"},  {"\\`{O}", "�"}, 
				    {"\\'O", "�"},  {"\\'{O}", "�"}, 
				    {"\\^O", "�"},  {"\\^{O}", "�"}, 
				    {"\\~O", "�"},  {"\\~{O}", "�"}, 
				    {"\\\"O", "�"}, {"\\\"{O}", "�"},
				    {"\\`U", "�"},  {"\\`{U}", "�"}, 
				    {"\\'U", "�"},  {"\\'{U}", "�"}, 
				    {"\\^U", "�"},  {"\\^{U}", "�"}, 
				    {"\\\"U", "�"}, {"\\\"{U}", "�"},
				    {"\\'Y", "�"},  {"\\'{Y}", "�"}, 
				    {"\\`a", "�"},  {"\\`{a}", "�"},
				    {"\\'a", "�"},  {"\\'{a}", "�"}, 
				    {"\\^a", "�"},  {"\\^{a}", "�"},
				    {"\\~a", "�"},  {"\\~{a}", "�"},
				    {"\\\"a", "�"}, {"\\\"{a}", "�"},
				    {"\\aa ", "�"}, {"\\aa", "�"}, {"\\a{a}", "�"},
				    {"\\c c", "�"}, {"\\cc ", "�"}, {"\\c{c}", "�"}, {"\\c{c}", "�"},
				    {"\\`e", "�"},  {"\\`{e}", "�"}, 
				    {"\\'e", "�"},  {"\\'{e}", "�"}, 
				    {"\\^e", "�"},  {"\\^{e}", "�"},
				    {"\\\"e", "�"}, {"\\\"{e}", "�"},
				    {"\\'i", "�"},  {"\\'{i}", "�"},
				    {"\\'\\i", "�"}, {"\\'{\\i}", "�"},
				    {"\\'i", "�"},  {"\\'{i}", "�"},
				    {"\\'\\i", "�"}, {"\\'{\\i}", "�"},
				    {"\\^i", "�"},  {"\\^{i}", "�"},
				    {"\\^\\i", "�"}, {"\\^{\\i}", "�"},
				    {"\\\"i", "�"}, {"\\\"{i}", "�"},
				    {"\\\"\\i", "�"}, {"\\\"{\\i}", "�"},
				    {"\\~n", "�"},  {"\\~{n}", "�"},
				    {"\\`o", "�"},  {"\\`{o}", "�"},
				    {"\\'o", "�"},  {"\\'{o}", "�"},
				    {"\\^o", "�"},  {"\\^{o}", "�"},
				    {"\\~o", "�"},  {"\\~{o}", "�"},
				    {"\\\"o", "�"}, {"\\\"{o}", "�"}, {"\\\" o", "�"},
				    {"\\`u", "�"},  {"\\`{u}", "�"},
				    {"\\'u", "�"},  {"\\'{u}", "�"},
				    {"\\^u", "�"},  {"\\^{u}", "�"},
				    {"\\\"u", "�"}, {"\\\"{u}", "�"},
				    {"\\~n", "�"},  {"\\~{n}", "�"}, 
				    {"\\`", "" },
				    {"\\'", "" },
				    {"\\^", "" },
				    {"\\~{ }", "~"},
				    {"\\~{}", "~"},
				    {"\\~", ""},
				    {"\\\"", ""},
				    {"\\.", ""},
				    {"\\=", ""},
				    {"\\*`", "" },
				    {"\\*'", "" },
				    {"\\*^", "" },
				    {"\\*~", ""},
				    {"\\v ", ""},   {"\\v", "" },
				    {"\\u", "" },
				    {"\\H ", ""},   {"\\H", "" },
				    {"\\oe", "oe"}, {"\\ae", "ae"},  
				    {"\\o", "o"},  
				    {"\\it", ""},  
				    {"\\i", "i"},   {"\\j", "j"},  
				    {"\\ss ", "ss"}, {"\\ss", "ss"},
				    {"\\c ", ""},
				    {"\\-", ""},    {"\\&", "&"},
				    {"\\{", "{"}, {"\\}", "}"},
				    {"\\#", "#"},
				    {"\\\\ ", " "}, {"\\ ", " "}, {"�", " "},
				    {"\\,", " "},
				    {"\\_", "_"},
				    {"\\/", ""},
				    {"\\\\xb4e", "�" },
				    {"\\em", ""},  {"\\tt", ""},  
				    {"\\bf", ""},  {"\\sc", ""},
				    {"\\textit", ""},
				    {"\\path", ""},
				    {"XXX", "XXX"} };



const char *latex_from_8bits[][2] = { {"�", "\\`{A}"},
				      {"�", "\\'{A}"}, 
				      {"�", "\\^{A}"}, 
				      {"�", "\\~{A}"},
				      {"�", "\\\"{A}"},
				      {"�","\\A{A}"},
				      {"�","\\`{E}"}, 
				      {"�","\\'{E}"},
				      {"�","\\^{E}"}, 
				      {"�", "\\\"{E}"}, 
				      {"�","\\`{I}"}, 
				      {"�","\\'{I}"}, 
				      {"�","\\^{I}"}, 
				      {"�", "\\\"{I}"}, 
				      {"�","\\~{N}"},
				      {"�","\\`{O}"}, 
				      {"�","\\'{O}"}, 
				      {"�","\\^{O}"}, 
				      {"�","\\~{O}"}, 
				      {"�", "\\\"{O}"},
				      {"�","\\`{U}"}, 
				      {"�","\\'{U}"}, 
				      {"�","\\^{U}"}, 
				      {"�", "\\\"{U}"},
				      {"�","\\'{Y}"}, 
				      {"�","\\`{a}"},
				      {"�","\\'{a}"}, 
				      {"�","\\^{a}"},
				      {"�","\\~{a}"},
				      {"�", "\\\"{a}"},
				      {"�","\\a{a}"},
				      {"�","\\c{c}"},
				      {"�","\\`{e}"}, 
				      {"�","\\'{e}"}, 
				      {"�","\\^{e}"},
				      {"�", "\\\"{e}"},
				      {"�","\\'{\\i}"},
				      {"�","\\'{\\i}"},
				      {"�","\\^{\\i}"},
				      {"�", "\\\"{\\i}"},
				      {"�","\\~{n}"},
				      {"�","\\`{o}"},
				      {"�","\\'{o}"},
				      {"�","\\^{o}"},
				      {"�","\\~{o}"},
				      {"�", "\\\"{o}"},
				      {"�","\\`{u}"},
				      {"�","\\'{u}"},
				      {"�","\\^{u}"},
				      {"�", "\\\"{u}"},
				      {"�","\\~{n}"}, 
				      {"&", "\\&"}, {"#", "\\#"}, {"-", "\\-"}, 
				      {"XXX", "XXX"} };



const char *upper_8bits_to_7bits[][2] = { {"�", "A"}, {"�", "A"},
					  {"�", "A"}, {"�", "A"},
					  {"�", "A"}, {"�", "A"},
					  {"�", "AE"}, {"�", "C"},
					  {"�", "E"}, {"�", "E"},
					  {"�", "E"}, {"�", "E"},
					  {"�", "I"}, {"�", "I"},
					  {"�", "I"}, {"�", "I"},
					  {"�", "D"}, {"�", "N"},
					  {"�", "O"}, {"�", "O"},
					  {"�", "�"}, {"�", "O"},
					  {"�", "U"}, {"�", "U"},
					  {"�", "U"}, {"�", "U"},
					  {"�", "Y"},
					  {"XXX", "XXX"} };

const char *lower_8bits_to_7bits[][2] = { {"�", "ss"},
					  {"�", "a"}, {"�", "a"},
					  {"�", "a"}, {"�", "a"},
					  {"�", "a"}, {"�", "a"}, 
					  {"�", "ae"}, {"�", "c"},
					  {"�", "e"}, {"�", "e"},
					  {"�", "e"}, {"�", "e"},
					  {"�", "i"}, {"�", "i"},
					  {"�", "i"}, {"�", "i"},
					  {"�", "n"}, {"�", "o"},
					  {"�", "o"}, {"�", "o"},
					  {"�", "o"}, {"�", "o"},
					  {"�", "o"}, {"�", "u"},
					  {"�", "u"}, {"�", "u"},
					  {"�", "u"}, {"�", "y"},
					  {"XXX", "XXX"} };

const char *key_unknown_name = "ZZZZZZZ-UNKNOWN";
enumIndexValue default_index_status = INDIFFERENT;










static void _print_bibtex_fields( FILE *f, int print_all_fields )
{
  int field;
  int maxlength;
  char format[20];
  
  fprintf( f, "\n" );
  fprintf( f, "=== Recognized bibtex fields === \n" );
  fprintf( f, "\n" );

  maxlength = strlen( desc_field[0] ); 
  for ( field=1; strncmp( desc_field[field], "XXX", 3 ) != 0 ; field ++ ) {
    if ( print_all_fields || desc_field_lgth[field] > 0 )
      if ( maxlength < strlen( desc_field[field] ) ) maxlength = strlen( desc_field[field] );
  }
  sprintf( format, "field #%%2d %%-%ds", maxlength );

  for ( field=0; strncmp( desc_field[field], "XXX", 3 ) != 0 ; field ++ ) {

    if ( field >= DESC_FIELD_NB ) {
      fprintf( stderr, "ERROR: it seems that there are more fields than 'DESC_FIELD_NB' ...\n" );
      fprintf( stderr, "   field #%d is '%s'\n", field, desc_field[field] );
      fprintf( stderr, "   DESC_FIELD_NB is %d\n", DESC_FIELD_NB );
      exit( -1 );
    }
    
    if ( print_all_fields || desc_field_lgth[field] > 0 ) {
      fprintf( f, format, field, desc_field[field] );
      fprintf( f, " has an allocated memory of %5d bytes\n", desc_field_lgth[field] );
    }

  }


  if ( print_all_fields ) {

    fprintf( f, "\n" );
    fprintf( f, "=== Ignored bibtex fields === \n" );
    fprintf( f, "\n" );
    
    maxlength = strlen( other_desc_field[0] ); 
    for ( field=1; strncmp( other_desc_field[field], "XXX", 3 ) != 0 ; field ++ ) {
      if ( maxlength < strlen( other_desc_field[field] ) ) maxlength = strlen( other_desc_field[field] );
    }
    sprintf( format, "field #%%2d %%-%ds", maxlength );

    for ( field=0; strncmp( other_desc_field[field], "XXX", 3 ) != 0 ; field ++ ) {
      fprintf( f, format, field, other_desc_field[field] );
      fprintf( f, "\n" );
    }
  }

  fprintf( f, "\n" );

}





void print_bibtex_fields( FILE *fd )
{
  _print_bibtex_fields( fd, 0 );
}





void print_bibtex_all_fields( FILE *fd )
{
  _print_bibtex_fields( fd, 1 );
}





static void _print_bibtex_templates( FILE *f, int print_all_fields )
{
  int item = 0;
  char *offset = "   ";
  int field;
  int l;

  for ( item=0; strncmp( desc_item[item], "XXX", 3 ) != 0 ; item ++ ) {

    switch ( item ) {
    default :
      break;
    case ARTICLE :
    case BOOK :
    case BOOKLET :
    case CONFERENCE :
    case INBOOK :
    case INCOLLECTION :
    case INPROCEEDINGS : 
    case MANUAL :
    case MASTERSTHESIS :
    case MISC :
    case PHDTHESIS :   
    case PROCEEDINGS :
    case TECHREPORT :
    case UNPUBLISHED :

    case PATENT :
    case STANDARD :

      fprintf( f, "@%s{\n", desc_item[item] );
    }

    for ( field=0; strncmp( desc_field[field], "XXX", 3 ) != 0 ; field ++ ) {

      if ( field > 0 ) 
	if ( print_all_fields || desc_fields_bibtex_item[item][field-1] != _IGNORED__ )
	  fprintf( f, ",\n" );

      switch( desc_fields_bibtex_item[item][field] ) {
      default :
      case _IGNORED__ :
	if ( print_all_fields )
	  fprintf( f, "%sIGNORED-%-4s = ", offset, desc_field[field] );
	break;
      case _REQUIRED_ :
	fprintf( f, "%s%-12s = ", offset, desc_field[field] );
	break;
      case _ALTERNAT_ :
	fprintf( f, "%sALT%-9s = ", offset, desc_field[field] );
	break;
      case _OPTIONAL_ :
	fprintf( f, "%sOPT%-9s = ", offset, desc_field[field] );
	break;
      }
      
      if ( print_all_fields || desc_fields_bibtex_item[item][field] != _IGNORED__ ) {	
	switch( field ) {
	default :
	  fprintf( f, "{}" );
	  break;

#if (defined(_INRIA_) || defined(_HALINRIA_))
	case XEDITORIALBOARD :
        case XPROCEEDINGS :
	case XINTERNATIONALAUDIENCE :
	fprintf( f, "{yes/no/undef}" );
	break;
	case XINVITEDCONFERENCE :
	case XSCIENTIFICPOPULARIZATION :
	  fprintf( f, "{yes}" );
	  break;
#endif

	}
      }
      
    }

    fprintf( f, "\n" );

    fprintf( f, "}\n" );
    fprintf( f, "\n" );

  }
}





void print_bibtex_templates( FILE *f ) {
  _print_bibtex_templates( f, 0 );
}





void print_bibtex_full_templates( FILE *f ) {
  _print_bibtex_templates( f, 1 );
}
