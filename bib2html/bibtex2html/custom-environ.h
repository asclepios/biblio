/*************************************************************************
 * custom-environ.h - from bibtex2html distribution
 *
 * $Id: custom-environ.h,v 2.5 2004/07/16 17:33:55 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */



#ifndef _custom_environ_h_
#define _custom_environ_h_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <common.h>

#include <custom-common.h>



/* what is needed to describe
   the global environment
   some index file particularities
 */

typedef struct {

  char *directory_authors;
  char *directory_biblio;
  char *directory_categories;
  char *directory_keywords;
  char *directory_years;

  char *directory_icons;

  char *iconsdir;

  char *filename_complete_biblio;
  char *filename_index;
  char *filename_extension;

  int use_html_links_between_pages;
  int use_html_links_towards_authors_pages;
  int use_html_links_towards_categories_pages;
  int use_html_links_towards_reduced_years_pages;
  int use_html_links_towards_complete_years_pages;
  int use_html_links_towards_keywords_pages;

  int use_html_links_towards_urls;

  int use_icons;

} typeEnvironmentDescription;




/* global constants
 */

extern char *default_environment_description[];

extern char *default_environment_description_html[];
extern char *default_environment_description_bibtex[];
extern char *default_environment_description_latex[];
extern char *default_environment_description_txt[];
extern char *default_environment_description_none[];



extern typeArgDescription environment_description[];



/* prototypes
 */
extern void init_environment_description( typeEnvironmentDescription *e );
extern void free_environment_description( typeEnvironmentDescription *e );
extern int add_environment_strings( typeArgDescription *custom,
				    int length,
				    typeEnvironmentDescription *e,
				    typeArgDescription *ed );

#ifdef __cplusplus
}
#endif

#endif
